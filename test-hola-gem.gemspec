# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'test/hola/gem/version'

Gem::Specification.new do |spec|
  spec.name          = "test-hola-gem"
  spec.version       = Test::Hola::Gem::VERSION
  spec.authors       = ["Jason"]
  spec.email         = ["jassun@blackberry.com"]
  spec.description   = "Following the guide from http://guides.rubygems.org/make-your-own-gem/"
  spec.summary       = "Introduce Jason (me) to making gems"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
